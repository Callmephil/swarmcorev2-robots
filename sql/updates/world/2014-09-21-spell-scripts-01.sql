DELETE FROM `spell_script_names` WHERE `spell_id` IN (-34861,64844,64904,54968,48438,17,73325);
INSERT INTO `spell_script_names` (`spell_id` ,`ScriptName`) VALUES
(-34861,'spell_pri_circle_of_healing'),
(64844, 'spell_pri_divine_hymn'),
(64904, 'spell_pri_hymn_of_hope'),
(17, 'spell_pri_body_and_soul'),
(73325, 'spell_pri_body_and_soul'),
(54968, 'spell_pal_glyph_of_holy_light'),
(48438,'spell_dru_wild_growth');