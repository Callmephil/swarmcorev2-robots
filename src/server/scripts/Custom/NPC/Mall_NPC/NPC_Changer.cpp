/*
<--------------------------------------------------------------------------->
- Developer(s): Ghostcrawler336
- Edited: Synth
- Complete: 100%
- ScriptName: 'npc_changer'
- Comment: Tested.
- Updated: 9/17/13.
<--------------------------------------------------------------------------->
*/
#include "ScriptPCH.h"

enum  defines
{
	faction_token = 110666, // Faction Change Token
	race_token = 110667, // Race Change Token
	customize_token = 110668,  // Customize Change Token
	rename_token = 110669, // Rename Change Token
};

#define CHANGEMYNAME_ICON "|TInterface\\icons\\INV_Letter_18:24:24:0:0|t "
#define CHANGEMYRACE_ICON "|TInterface\\icons\\Ability_Rogue_Disguise:24:24:0:0|t "
#define CHANGEMYFACTION_ICON "|TInterface\\icons\\Achievement_BG_winWSG:24:24:0:0|t "
#define CUSTOMIZEMYCHAR_ICON "|TInterface\\icons\\INV_Misc_GroupLooking:24:24:0:0|t "

#define CHANGEMYNAME_STRING " Change Name"
#define CHANGEMYRACE_STRING " Change Race"
#define CHANGEMYFACTION_STRING " Change Faction"
#define CUSTOMIZEMYCHAR_STRING " Customize My Character"


class npc_changer : public CreatureScript
{
public:
	npc_changer() : CreatureScript("npc_changer"){}

	bool OnGossipHello(Player * player, Creature * pCreature)
	{
		
		player->ADD_GOSSIP_ITEM(-1, CHANGEMYNAME_ICON CHANGEMYNAME_STRING, GOSSIP_SENDER_MAIN, 3);
		player->ADD_GOSSIP_ITEM(-1, CHANGEMYRACE_ICON CHANGEMYRACE_STRING, GOSSIP_SENDER_MAIN, 0);
		player->ADD_GOSSIP_ITEM(-1, CHANGEMYFACTION_ICON CHANGEMYFACTION_STRING, GOSSIP_SENDER_MAIN, 1);
        player->ADD_GOSSIP_ITEM(-1, CUSTOMIZEMYCHAR_ICON CUSTOMIZEMYCHAR_STRING, GOSSIP_SENDER_MAIN, 2);
		
		player->SEND_GOSSIP_MENU(9425, pCreature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player * player, Creature * Creature, uint32 /*uiSender*/, uint32 action)
	{
		if (!player)
			return true;

		switch (action)
		{
		case 0:
			if (player->HasItemCount(race_token, 1))
			{
				player->DestroyItemCount(race_token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
				player->GetSession()->SendNotification("You need to relog, to change your race!");
				player->CLOSE_GOSSIP_MENU();
			}
			else
			{
				player->GetSession()->SendNotification("You need atleast one token!");
				player->CLOSE_GOSSIP_MENU();
			}
			break;
		case 1:
			if (player->HasItemCount(faction_token, 1))
			{
				player->DestroyItemCount(faction_token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
				player->GetSession()->SendNotification("You need to relog, to change your faction!");
				player->CLOSE_GOSSIP_MENU();
			}
			else
			{
				player->GetSession()->SendNotification("You need atleast one tokens!");
				player->CLOSE_GOSSIP_MENU();
			}
			break;
		case 2:
			if (player->HasItemCount(customize_token, 1))
			{
				player->DestroyItemCount(customize_token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
				player->GetSession()->SendNotification("You need to relog, to customize yourself!");
				player->CLOSE_GOSSIP_MENU();

			}

			else
			{
				player->GetSession()->SendNotification("You need atleast one token!");
				player->CLOSE_GOSSIP_MENU();
			}
			break;
		case 3:
			if (player->HasItemCount(rename_token, 1))
			{
				player->DestroyItemCount(rename_token, 1, true, false);
				player->SetAtLoginFlag(AT_LOGIN_RENAME);
				player->GetSession()->SendNotification("You need to relog, to change your name!");
				player->CLOSE_GOSSIP_MENU();
			}

			else
			{
				player->GetSession()->SendNotification("You need atleast one token!");
				player->CLOSE_GOSSIP_MENU();
			}
			break;
		}
		return true;
	}

};

void AddSC_npc_changer()
{
	new npc_changer();
}