#include "Arena_1v1.h"

class npc_1v1arena : public CreatureScript
{
public:
	npc_1v1arena() : CreatureScript("npc_1v1arena") {}

	bool OnGossipHello(Player* player, Creature* me)
	{
		if (!player || !me)
			return true;
		if (sWorld->getBoolConfig(CONFIG_ARENA_1V1_ENABLE) == true)
		{
			ChatHandler(player->GetSession()).SendSysMessage("|cffff00001v1 is currently disabled!");
			return true;
		}

		{
			if (!player)
				return false;

			if (sWorld->getBoolConfig(CONFIG_ARENA_1V1_BLOCK_FORBIDDEN_TALENTS) == false)
				return true;

			std::stringstream err;

			if (player->GetFreeTalentPoints() > 0)
				err << "|cffff0000You currently have " << player->GetFreeTalentPoints() << " free talent points. Please spend all your talent points before queueing arena.\n";

			Item* newItem = NULL;
			for (uint8 slot = EQUIPMENT_SLOT_START; slot < EQUIPMENT_SLOT_END; ++slot)
			{
				if (slot == EQUIPMENT_SLOT_OFFHAND || slot == EQUIPMENT_SLOT_RANGED || slot == EQUIPMENT_SLOT_TABARD || slot == EQUIPMENT_SLOT_BODY)
					continue;

				newItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot);
				if (newItem == NULL)
				{
					err << "|cffff0000Your character is not fully equipped.\n";
					break;
				}
			}

			if (err.str().length() > 0)
			{
				ChatHandler(player->GetSession()).SendSysMessage(err.str().c_str());
				return false;
			}
			else

				if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5))
					player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|TInterface\\icons\\achievement_bg_winwsg:30|t Leave queue for 1v1 Arena", GOSSIP_SENDER_MAIN, 3, "Are you sure?", 0, false);

			if (player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5)) == NULL)
				player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|TInterface\\icons\\achievement_bg_winwsg:30|t Create a new 1v1 Arena Team", GOSSIP_SENDER_MAIN, 1, "Create a new 1v1 Arena Team?", sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS), false);
			else
			{
				if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5) == false)
				{
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\icons\\achievement_bg_winwsg:30|t Sign up for 1v1 Arena!", GOSSIP_SENDER_MAIN, 2);
					player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT, "|TInterface\\icons\\achievement_bg_winwsg:30|t Disband Arena Team", GOSSIP_SENDER_MAIN, 5, "Are you sure?", 0, false);
				}

				player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "|TInterface\\icons\\achievement_bg_winwsg:30|t Show Statistics", GOSSIP_SENDER_MAIN, 4);
			}

			player->SEND_GOSSIP_MENU(68, me->GetGUID());
			return true;
		}
	}

	bool OnGossipSelect(Player* player, Creature* me, uint32 /*uiSender*/, uint32 uiAction)
	{
		if (!player || !me)
			return true;

		player->PlayerTalkClass->ClearMenus();

		switch (uiAction)
		{
		case 1: // Create new Arenateam
		{
			if (sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL) <= player->getLevel())
			{
				if (player->GetMoney() >= sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) && CreateArenateam(player, me))
					player->ModifyMoney(sWorld->getIntConfig(CONFIG_ARENA_1V1_COSTS) * -0);
			}
			else
			{
				ChatHandler(player->GetSession()).PSendSysMessage("You need level %u+ to create an 1v1 arena team.", sWorld->getIntConfig(CONFIG_ARENA_1V1_MIN_LEVEL));
				player->CLOSE_GOSSIP_MENU();
				return true;
			}
		}
		break;

		case 2: // Join Queue Arena (rated)
		{
			if (Arena1v1CheckTalents(player) && JoinQueueArena(player, me, true) == false)
				ChatHandler(player->GetSession()).SendSysMessage("Something went wrong while joining queue.");

			player->CLOSE_GOSSIP_MENU();
			return true;
		}
		break;

		case 3: // Leave Queue
		{
			uint64 guid = player->GetGUID();
			uint8 arenatype = ARENA_TYPE_5v5; // 2v2, 3v3 or 5v5

			//recvData >> arenaslot;

			//uint8 arenatype = ArenaTeam::GetTypeBySlot(arenaslot);
			Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
			BattlegroundTypeId bgTypeId = bg->GetTypeID();
			BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
			BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
			if (player->InBattlegroundQueueForBattlegroundQueueType(BATTLEGROUND_QUEUE_5v5))
			{
				uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);
				queueSlot = player->GetBattlegroundQueueIndex(BATTLEGROUND_QUEUE_5v5);
				uint8 arenaType = ARENA_TYPE_5v5;

				WorldPacket Data;
				Data << (uint32)0x0 << queueSlot << arenaType << player->GetGUID() << (uint8)0x0;
				player->GetSession()->HandleBattleFieldPortOpcode(Data);
				player->CLOSE_GOSSIP_MENU();
				return true;
			}
		}
		break;

		case 4: // get statistics
		{
			ArenaTeam* at = sArenaTeamMgr->GetArenaTeamById(player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5)));
			if (at)
			{
				std::stringstream s;
				s << "Rating: " << at->GetStats().Rating;
				s << "\nRank: " << at->GetStats().Rank;
				s << "\nSeason Games: " << at->GetStats().SeasonGames;
				s << "\nSeason Wins: " << at->GetStats().SeasonWins;
				s << "\nWeek Games: " << at->GetStats().WeekGames;
				s << "\nWeek Wins: " << at->GetStats().WeekWins;

				ChatHandler(player->GetSession()).PSendSysMessage(s.str().c_str());
			}
		}
		break;


		case 5: // Disband arenateam
		{
			WorldPacket Data;
			Data << (uint32)player->GetArenaTeamId(ArenaTeam::GetSlotByType(ARENA_TEAM_5v5));
			player->GetSession()->HandleArenaTeamLeaveOpcode(Data);
			ChatHandler(player->GetSession()).SendSysMessage(" Your Arena team has been deleted!");
			player->CLOSE_GOSSIP_MENU();
			return true;
		}
		break;
		}

		OnGossipHello(player, me);
		return true;
	}
};

void AddSC_npc_1v1arena()
{
	new npc_1v1arena();
}